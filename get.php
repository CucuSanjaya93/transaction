<?php
require 'db.php';
$connect = connect();
$user = array();
$sql = "SELECT USER_ID, TRX_ID, NAME, ITEM, QTY, IMAGE  FROM tb_user";
if ($result = mysqli_query($connect, $sql)) {
    $count = mysqli_num_rows($result);
 
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $user[$i]['USER_ID'] = $row['USER_ID'];
        $user[$i]['TRX_ID'] = $row['TRX_ID'];
        $user[$i]['NAME'] = $row['NAME'];
        $user[$i]['ITEM'] = $row['ITEM'];
		$user[$i]['IMAGE'] = $row['IMAGE'];
        $i++;
    }
}
 
$json = json_encode($user);
echo $json;
exit;
?>