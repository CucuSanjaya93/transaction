var simpleApp = angular.module('simpleApp', []);
 
simpleApp.controller('mainController',function($scope,$http){
    $scope.users;
 
    $scope.getUser = function() {
          $http({
              method: 'GET',
              url: 'get.php'
          }).then(function (response) {
              // success
              $scope.users = response.data;
          }, function (response) {
              // error
              console.log(response.data,response.status);
          });
    };
 
    $scope.addUser = function() {
          $http({
               method: 'POST',
               url:  'insert.php',
               data: {newTrxId: $scope.newTrxId, newName: $scope.newName, newItem: $scope.newItem, newImage: $scope.newImage}
          }).then(function (response) {
               //success
               $scope.getUser();
          }, function (response) {
              //error
               console.log(response.data,response.status);
          });
    };
 
    $scope.deleteUser = function( USER_ID ) {
          $http({
              method: 'POST',
              url:  'delete.php',
              data: { recordId : USER_ID }
          }).then(function (response) {
              $scope.getUser();
          }, function (response) {
              console.log(response.data,response.status);
          });
        };
        
    $scope.reset = function(){
            $scope.newTrxId = "";
            $scope.newName = "";
			$scope.newItem = "";
			$scope.newImage = "";
        }
        
    $scope.getUser();
});